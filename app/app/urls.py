from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.http import HttpResponse

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/user/', include('user.urls')),
    path('api/post/', include('post.urls')),
    path('health/', lambda r: HttpResponse(
        'ok', status=200
    )),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
